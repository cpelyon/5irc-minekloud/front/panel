export const ACTION_SHOW = 'alert/show'
export const ACTION_HIDE = 'alert/hide'

export const showAlertAction = (type, message, duration) => {
  return {
    type: ACTION_SHOW,
    value: {
      type,
      message,
      duration
    },
  }
}

export const showSuccessAlertAction = (message, duration) => showAlertAction("success", message, duration)

export const showErrorAlertAction = (message, duration) => showAlertAction("error", message, duration)

export const showWarningAlertAction = (message, duration) => showAlertAction("warning", message, duration)

export const showInfoAlertAction = (message, duration) => showAlertAction("info", message, duration)

export const hideAlertAction = () => {
  return {
    type: ACTION_HIDE,
  }
}