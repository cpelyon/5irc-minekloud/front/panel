import {ACTION_HIDE, ACTION_SHOW} from "../actions/alertActions";

const initialState = {
  value: null
}

export default function alertReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_SHOW:
      return {
        ...state,
        value: action.value,
      }
    case ACTION_HIDE:
      return {
        ...state,
        value: null,
      }
    default:
      return state
  }
}