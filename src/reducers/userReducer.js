import {ACTION_LOGIN, ACTION_LOGOUT} from "../actions/userActions";
import StorageService from "../services/Storage.service";

const initialState = {
  value: StorageService.loadUser()
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_LOGIN:
      StorageService.saveUser(action.value)
      return {
        ...state,
        value: action.value,
      }
    case ACTION_LOGOUT:
      StorageService.clearUser()
      return {
        ...state,
        value: null,
      }
    default:
      return state
  }
}