import {showErrorAlertAction} from "../actions/alertActions";

export default class APIService {
  static fetch(dispatch, request, resultConvertor = async res => await res) {
    const service = new APIService(dispatch, request)
    service.fetch(resultConvertor)
    return service
  }

  static fetchJson(dispatch, request) {
    return APIService.fetch(dispatch, request,
      res => res.json().catch(() => {
        throw new Error("A server error occured.")
      }))
  }

  constructor(dispatch, request) {
    this.dispatch = dispatch
    this.request = request
    this._success = []
    this._error = []
    this._finally = []
  }

  success(success) {
    this._success.push(success)
    return this
  }

  error(error) {
    this._error.push(error)
    return this
  }

  finally(_finally) {
    this._finally.push(_finally)
    return this
  }

  async fetch(resultConvertor) {
    try {
      await this.request // obligé d'utiliser await sinon le finally n'est pas exécuté
        .catch(() => {
          throw new Error("A connection error occured.")
        })
        .then(async response => {
          if (response.ok) {
            for (const success1 of this._success) {
              await success1(response, () => resultConvertor(response))
            }
            return
          }
          for (const error1 of this._error) {
            await error1(response, () => resultConvertor(response))
          }
        })
    } catch (e) {
      console.error(e)
      this.dispatch(showErrorAlertAction(e.message, 4000))
    } finally {
      this._finally.forEach(_finally => _finally())
    }
  }
}