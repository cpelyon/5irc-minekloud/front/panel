import React from "react";
import {ServerStatus} from "./Server.service";
import {ConfigEnum, ConfigService} from "./Config.service";

export class MetricsService {
  static async getRange(serverId, token, start, end, offset, unit) {
    const [players, status, cpu, ram] = await Promise.all([
      MetricsService.get(serverId, token, 'range', 'players', start, end, offset, unit),
      MetricsService.get(serverId, token, 'range', 'status', start, end, offset, unit),
      MetricsService.get(serverId, token, 'range', 'cpu', start, end, offset, unit),
      MetricsService.get(serverId, token, 'range', 'ram', start, end, offset, unit)
    ])
    return {players, status, cpu, ram}
  }

  static async get(serverId, token, scale, type, start, end, offset, unit) {
    const apiUrl = await ConfigService.get(ConfigEnum.METRICS_API_URL)
    return fetch(`${apiUrl}/api/metrics/${scale}/servers/${serverId}/${type}?start=${start}&end=${end}&offset=${offset}&unit=${unit}`, {
      headers: {
        Accept: 'application/json',
        Authorization: "Bearer " + token,
      },
    }).then(res => res.json())
  }
}

export const Status = {
  OFF: 'OFF',
  STARTING: 'STARTING',
  ON: 'ON',
  STOPPING: 'STOPPING',
  UNKNOWN: 'UNKNOWN',
}

export const ServerHealth = {
  HEALTHY: 'healthy',
  UNHEALTHY: 'unhealthy',
}

export function getStatus(status, health) {
  if (status === ServerStatus.ON) {
    switch (health) {
      case ServerHealth.HEALTHY:
        return Status.ON
      case ServerHealth.UNHEALTHY:
        return Status.STARTING
      default:
        return Status.UNKNOWN
    }
  } else {
    switch (health) {
      case ServerStatus.HEALTHY:
        return Status.OFF
      default:
        return Status.STOPPING
    }
  }
}