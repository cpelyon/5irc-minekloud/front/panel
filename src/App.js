import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {Grid, Toolbar} from "@material-ui/core";
import MenuBar from "./components/MenuBar";
import Login from "./components/Login";
import Register from "./components/Register";
import Logout from "./components/Logout";
import Panel from "./components/panel/Panel";
import React from "react";
import AppAlert from "./components/AppAlert";

export function App() {
  return (
    <Router>
      <Grid container>
        <MenuBar/>
      </Grid>
      <Toolbar/>
      <Switch>
        <Route path="/login">
          <Login/>
        </Route>
        <Route path="/register">
          <Register/>
        </Route>
        <Route path="/logout">
          <Logout/>
        </Route>
        <Route path="/panel">
          <Panel/>
        </Route>
        <Route exact path="/">
          <Redirect to="/panel"/>
        </Route>
      </Switch>
      <AppAlert/>
    </Router>
  )
}
