import LogConsole from "./LogConsole";
import {NavLink, Redirect, Route, Switch, useRouteMatch} from "react-router-dom";
import React, {useEffect} from "react";
import {
  CircularProgress,
  Container,
  Divider,
  Drawer,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography
} from "@material-ui/core";
import {Description, People, PowerSettingsNew, Settings, TrendingUp} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import ServerInfo from "./ServerInfo";
import Configuration from "./Configuration";
import Players from "./Players";
import StorageService from "../../../services/Storage.service";
import APIService from "../../../services/API.service";
import {ServerService} from "../../../services/Server.service";
import {updateServerAction} from "../../../actions/serverActions";
import {showErrorAlertAction} from "../../../actions/alertActions";
import {getStatus} from "../../../services/Metrics.service";
import ServerStatusIndicator from "./status/ServerStatusIndicator";
import Projections from "./Projections";

const drawerWidth = 300;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    marginLeft: drawerWidth
  },
  drawerHeader: {
    justifyContent: "center",
  },
  drawerHeaderText: {
    fontWeight: "bold",
  },
}));

const links = [
  {
    title: "Status",
    icon: <PowerSettingsNew/>,
    path: "/panel/server/status",
  },
  {
    title: "Configuration",
    icon: <Settings/>,
    path: "/panel/server/configuration",
  },
  {
    title: "Console",
    icon: <Description/>,
    path: "/panel/server/console",
  },
  {
    title: "Players",
    icon: <People/>,
    path: "/panel/server/players",
  },
  {
    title: "Projections",
    icon: <TrendingUp/>,
    path: "/panel/server/projections",
  },
]

export default function ServerPanel() {
  const token = useSelector((state) => state.userReducer).token
  const server = useSelector((state) => state.serverReducer).value
  const health = useSelector(state => state.metricsReducer.health).find(h => h.serverId === server?.id)
  const match = useRouteMatch()
  const dispatch = useDispatch()
  const classes = useStyles()

  useEffect(() => {
    const interval = setInterval(() => APIService.fetchJson(dispatch, ServerService.get(serverId, token))
      .success((_, json) => json().then(res => dispatch(updateServerAction(res))))
      .error(() => dispatch(showErrorAlertAction("An error occured while fetching the server.", 4000))), 1000)
    return () => clearInterval(interval)
  }, [])

  const serverId = StorageService.loadServer()
  if (server === null) {
    if (serverId === null) {
      return <Redirect to="/panel"/>
    }
    return <LoadingScreen/>
  }

  const status = getStatus(server.status, health?.status)

  return (
    <div>
      <Drawer className={classes.drawer} variant="permanent" classes={{paper: classes.drawerPaper,}}>
        <Toolbar/>
        <div className={classes.drawerContainer}>
          <List>
            <ListItem className={classes.drawerHeader}>
              <Typography variant="h5" className={classes.drawerHeaderText}>{server.name}</Typography>
            </ListItem>
            <ListItem className={classes.drawerHeader}>
              <ServerStatusIndicator status={status}/>
            </ListItem>
          </List>
          <Divider/>
          <List>
            {links.map((value, index) => (
              <NavLink key={index} to={value.path}>
                <ListItem button>
                  <ListItemIcon>{value.icon}</ListItemIcon>
                  <ListItemText primary={value.title}/>
                </ListItem>
              </NavLink>
            ))}
          </List>
        </div>
      </Drawer>
      <div className={classes.content}>
        <Switch>
          <Route path={`${match.path}/status`}>
            <ServerInfo/>
          </Route>
          <Route path={`${match.path}/configuration`}>
            <Configuration/>
          </Route>
          <Route path={`${match.path}/console`}>
            <LogConsole/>
          </Route>
          <Route path={`${match.path}/players`}>
            <Players/>
          </Route>
          <Route path={`${match.path}/projections`}>
            <Projections/>
          </Route>
          <Route path={`${match.path}`}>
            <Redirect to={`${match.path}/status`}/>
          </Route>
        </Switch>
      </div>
    </div>
  )
}

function LoadingScreen() {
  return (
    <Container>
      <Grid container justify="center">
        <Grid item>
          <CircularProgress size={100} style={{marginTop: 100}}/>
        </Grid>
      </Grid>
    </Container>
  )
}