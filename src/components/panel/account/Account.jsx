import {Button, Container, Grid, Paper, TextField, Typography} from "@material-ui/core";
import React, {useState} from "react";
import {useSelector} from "react-redux";
import PanelTitle from "../PanelTitle";
import {makeStyles} from "@material-ui/core/styles";
import {Edit, Save} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    height: "100%",
  },
}))

export default function Account() {
  const userData = useSelector(state => state.userReducer).value

  const [formDisabled, setFormDisabled] = useState(true)
  const [username, setUsername] = useState(userData.user.username)
  const [email, setEmail] = useState(userData.user.email)
  const [password, setPassword] = useState("")
  const [repassword, setRepassword] = useState("")
  const [usernameError, setUsernameError] = useState(null)
  const [emailError, setEmailError] = useState(null)
  const [passwordError, setPasswordError] = useState(null)
  const [repasswordError, setRepasswordError] = useState(null)

  const classes = useStyles()

  const handleUsernameChange = (event) => {
    let newUsername = event.target.value
    setUsername(newUsername)
    if (usernameError !== null && username !== newUsername && newUsername !== "") {
      setUsernameError(null)
    }
  }

  const handleEmailChange = (event) => {
    let newEmail = event.target.value
    setEmail(newEmail)
    if (emailError !== null && email !== newEmail && newEmail !== "") {
      setEmailError(null)
    }
  }

  const handlePasswordChange = (event) => {
    let newPassword = event.target.value
    setPassword(newPassword)
    if (passwordError !== null && password !== newPassword && newPassword !== "") {
      setPasswordError(null)
    }
  }

  const handleRepasswordChange = (event) => {
    let newRepassword = event.target.value
    setRepassword(newRepassword)
    if (repasswordError !== null && repassword !== newRepassword && newRepassword !== "") {
      setRepasswordError(null)
    }
  }

  const onUpdateUser = (event) => {
    event.preventDefault() // TODO
  }

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Account"/>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <Paper className={classes.paper}>
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
              Account details
            </Typography>
            <form className={classes.root} onSubmit={onUpdateUser}>
              <TextField margin="normal" fullWidth required variant="outlined" id="standard-username" label="Username"
                         value={username} onChange={handleUsernameChange}
                           disabled={formDisabled} error={usernameError !== null} helperText={usernameError}/>
                <TextField margin="normal" fullWidth required variant="outlined" type="email" id="standard-email"
                           label="Email" value={email} onChange={handleEmailChange}
                           disabled={formDisabled} error={emailError !== null} helperText={emailError}/>
                <TextField margin="normal" fullWidth variant="outlined" type="password" id="standard-password"
                           value={password} label="Password" onChange={handlePasswordChange}
                           disabled={formDisabled} error={passwordError !== null} helperText={passwordError}/>
                <TextField margin="normal" fullWidth variant="outlined" type="password" id="standard-re-password"
                           value={repassword} label="Re-Password" onChange={handleRepasswordChange}
                           disabled={formDisabled} error={repasswordError !== null} helperText={repasswordError}/>
              </form>
              <Grid container spacing={3}>
                <Grid item>
                  <Typography variant="body2" color="textSecondary">
                    Leave empty if you don't want to change your password.
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center" style={{marginTop: 20}}>
                <Grid item>
                  <Button margin="normal" fullWidth variant="outlined" size="large"
                          startIcon={<Edit/>} onClick={() => setFormDisabled(false)} disabled={!formDisabled}>
                    Edit details
                  </Button>
                </Grid>
                <Grid item>
                  <Button type="submit" margin="normal" fullWidth variant="contained" color="primary" size="large"
                          startIcon={<Save/>} onClick={onUpdateUser} disabled={formDisabled}>
                    Save
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={4} lg={3}>
            <Paper className={classes.paper}>
              <Typography component="h2" variant="h6" color="primary" gutterBottom>
                Technical data
              </Typography>
              <TextField margin="normal" fullWidth label="ID"
                         disabled value={userData.user.id}/>
              <TextField margin="normal" fullWidth label="Token"
                         disabled value={userData.token}/>
            </Paper>
          </Grid>
      </Grid>
    </Container>
  )
}