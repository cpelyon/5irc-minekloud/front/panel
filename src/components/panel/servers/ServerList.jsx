import React, {useState} from "react";
import {
  Button,
  CircularProgress,
  Container,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import {ServerRow} from './ServerRow'
import {AddBox} from "@material-ui/icons";
import {useSelector} from "react-redux";
import CreateServerDialog from "./CreateServerDialog";
import PanelTitle from "../PanelTitle";

const useStyle = makeStyles(() => ({
  table: {
    width: "100%",
  },
}));

export default function ServerList() {
  const classes = useStyle();
  const userServerList = useSelector((state) => state.serverListReducer)

  const [openDialog, setOpenDialog] = useState(0)

  return (
    <Container maxWidth="lg">
      <PanelTitle text="Servers"/>
      <TableContainer component={Paper}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Hostname</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Max. RAM</TableCell>
              <TableCell>Off/On</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userServerList.value?.map((server) => (
              <ServerRow key={server.id} server={server}/>
            ))}
            {userServerList.isUpdating && (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <CircularProgress/>
                </TableCell>
              </TableRow>
            )}
            {!userServerList.isUpdating && userServerList.value && userServerList.value.length === 0 && (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <Typography>
                    No server found.
                  </Typography>
                </TableCell>
              </TableRow>
            )}
            <TableRow>
              <TableCell colSpan={6} align="center">
                <Button variant="contained" color="default" size="large"
                        className={classes.button} startIcon={<AddBox/>}
                        onClick={() => setOpenDialog(prev => prev + 1)}>
                  Create a server
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <CreateServerDialog openDialog={openDialog}/>
    </Container>
  )

}
