import React, {useEffect, useState} from "react";
import {Container, makeStyles} from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";
import ServerList from './servers/ServerList';
import {Redirect, Route, Switch, useRouteMatch} from "react-router-dom";
import ServerPanel from "./server/ServerPanel";
import Account from "./account/Account";
import Billing from "./billing/Billing";
import SocketService from "../../services/Socket.service";
import {connectMetrics, disconnectMetrics, updateMetricsAction} from "../../actions/metricsActions";
import APIService from "../../services/API.service";
import {ServerService} from "../../services/Server.service";
import {updateServerListAction} from "../../actions/serverListActions";

const useStyle = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
}));

export default function Panel() {
  const userData = useSelector((state) => state.userReducer).value
  const userServerList = useSelector((state) => state.serverListReducer)
  const metricsServers = useSelector((state) => state.metricsReducer.serverList)
  const metricsConnected = useSelector((state) => state.metricsReducer.connected)
  const [socket, setSocket] = useState(null)
  const dispatch = useDispatch()
  const classes = useStyle();
  const match = useRouteMatch()

  useEffect(() => {
    if (userData !== null && userServerList.isUpdating) {
      updateServerList()
    } else if (userServerList.value?.length > 0) {
      const serversId = userServerList.value.map(s => s.id)
      if (serversId.toString() !== metricsServers.toString()) {
        connectSocket(serversId)
      }
    }
  }, [userServerList.isUpdating])

  const updateServerList = () => {
    APIService.fetchJson(dispatch, ServerService.getList(userData.user.id, userData.token))
      .success(async (_, json) => await json().then(res => dispatch(updateServerListAction(res))))
      .error(() => {
        throw new Error("A server error occured while loading the server list.")
      })
  }

  const connectSocket = async (serversId) => {
    SocketService.connectMetricsSocket(userData.user.id, serversId, userData.token).then(socket => {
      socket.on("connect", () => {
        dispatch(connectMetrics(serversId))
      })
      socket.on("disconnect", () => {
        socket.disconnect()
        setSocket(null)
        dispatch(disconnectMetrics())
      });
      socket.on("server-status-metric", handleNewMetrics);
      setSocket(socket)
    })
  }

  useEffect(() => {
    if (!socket) return
    socket.connect()

    return () => {
      SocketService.disconnectMetricsSocket()
    }
  }, [metricsConnected])

  const handleNewMetrics = (data) => {
    dispatch(updateMetricsAction(data))
  }

  if (userData === null) {
    return <Redirect to="/login"/>
  }
  return (
    <Container className={classes.root} maxWidth="xl" disableGutters>
      <Switch>
        <Route path={`${match.path}/server`}>
          <ServerPanel/>
        </Route>
        <Route path={`${match.path}/list`}>
          <ServerList/>
        </Route>
        <Route path={`${match.path}/account`}>
          <Account/>
        </Route>
        <Route path={`${match.path}/billing`}>
          <Billing/>
        </Route>
        <Route path={`${match.path}`}>
          <Redirect to={`${match.path}/list`}/>
        </Route>
      </Switch>
    </Container>
  )

}