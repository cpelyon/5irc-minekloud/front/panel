import React, {useState} from "react";
import {Avatar, Box, Button, Container, CssBaseline, Grid, makeStyles, TextField, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";
import {loginAction} from "../actions/userActions";
import {NavLink, Redirect} from "react-router-dom";
import Copyright from "./Copyright";
import UserService from "../services/User.service";
import {VpnKey} from "@material-ui/icons";
import {showErrorAlertAction} from "../actions/alertActions";
import APIService from "../services/API.service";

const useStyle = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  test: {
    padding: theme.spacing(8, 0, 6)
  }
}));

export default function Login() {
  const [username, setUsername] = useState("")
  const [usernameError, setUsernameError] = useState(null)
  const [password, setPassword] = useState("")
  const [passwordError, setPasswordError] = useState(null)
  const [buttonDisabled, setButtonDisabled] = useState(false)
  const userData = useSelector((state) => state.userReducer).value

  const classes = useStyle()
  const dispatch = useDispatch()

  const handleUsernameChange = (event) => {
    let newUsername = event.target.value
    setUsername(newUsername)
    if (usernameError !== null && username !== newUsername && newUsername !== "") {
      setUsernameError(null)
    }
  }

  const handlePasswordChange = (event) => {
    let newPassword = event.target.value
    setPassword(newPassword);
    if (passwordError !== null && password !== newPassword && newPassword !== "") {
      setPasswordError(null)
    }
  }

  const validateUsername = () => {
    if (username === "") {
      setUsernameError("Please enter your username.")
      return false
    }
    return true
  }

  const validatePassword = () => {
    if (password === "") {
      setPasswordError("Please enter your password.")
      return false
    }
    return true
  }

  const validateForm = () => [validateUsername(), validatePassword()].every(Boolean)

  const onLoginClick = (e) => {
    e.preventDefault()
    clearErrors()
    if (!validateForm()) return
    setButtonDisabled(true)

    APIService.fetchJson(dispatch, UserService.login(username, password))
      .success(onLoginSuccess)
      .error(onLoginError)
      .finally(() => setButtonDisabled(false))
  }

  const onLoginSuccess = async (_, json) => {
    await json().then(res => dispatch(loginAction({
      user: res.user,
      token: res.access_token
    })))
  }

  const onLoginError = async (response, json) => {
    if (response.status === 403) {
      throw new Error("Login has been disabled by Minekloud administrators.")
    }
    if (response.status === 401) {
      setPasswordError("Authentication failed. Please check your credentials.")
      return
    }
    await json().then(err => dispatch(showErrorAlertAction(err.message, 4000)))
  }

  const onCancelClick = () => {
    setUsername("")
    setPassword("")
    clearErrors()
  }

  const clearErrors = () => {
    setUsernameError(null)
    setPasswordError(null)
  }

  if (userData != null) {
    return <Redirect to="/panel"/>
  }
  return (
    <Container maxWidth="xs" className={classes.test}>
      <CssBaseline/>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <VpnKey/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form className={classes.form} onSubmit={onLoginClick}>
          <TextField required fullWidth variant="outlined" margin="normal" id="standard-username" value={username}
                     onChange={handleUsernameChange} label="Username"
                     error={usernameError !== null} helperText={usernameError}/>
          <TextField required fullWidth variant="outlined" margin="normal" type="password" id="standard-password"
                     value={password} onChange={handlePasswordChange} label="Password"
                     error={passwordError !== null} helperText={passwordError}/>
          <Grid container spacing={3}>
            {/* <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid> */}
            <Grid item>
              <NavLink to="/register">
                <Typography variant="body2" color="primary" className="MuiLink-underlineHover">
                  Don't have an account? Sign Up
                </Typography>
              </NavLink>
            </Grid>
          </Grid>
          {/* <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" /> */}
          <Grid container spacing={3} justify="center" style={{marginTop: 20}}>
            <Grid item>
              <Button type="submit" margin="normal" fullWidth variant="contained" color="primary" size="large"
                      startIcon={<VpnKey/>} onClick={onLoginClick} disabled={buttonDisabled}>
                Login
              </Button>
            </Grid>
            <Grid item>
              <Button margin="normal" fullWidth variant="outlined" size="large" onClick={onCancelClick}>
                Clear
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright/>
      </Box>
    </Container>
  )

}