import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import {CloudCircle, Dns, ExitToApp, PersonAdd, VpnKey} from "@material-ui/icons";
import {Button, Divider, Hidden, ListItemIcon, ListItemText} from "@material-ui/core";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'flex',
    alignItems: 'center',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
      alignItems: 'center',
    },
  },
  sectionDesktop2: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      alignItems: 'center',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

const menuItemsMain = (isUserLoggedIn) => {
  return isUserLoggedIn ?
    [
      {
        title: "Servers",
        icon: <Dns/>,
        path: "/panel/list",
      },
      {
        title: "Account",
        icon: <AccountCircle/>,
        path: "/panel/account",
      },
      /*{
        title: "Billing",
        icon: <AccountBalance/>,
        path: "/panel/billing",
      },*/
    ] :
    []
}

const menuItemsSecondary = (isUserLoggedIn) => {
  return isUserLoggedIn ?
    [
      {
        title: "Logout",
        icon: <ExitToApp/>,
        path: "/logout",
      },
    ] :
    [
      {
        title: "Login",
        icon: <VpnKey/>,
        path: "/login"
      },
      {
        title: "Register",
        icon: <PersonAdd/>,
        path: "/register"
      },
    ]
}

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu{...props}/>
));

export default function MenuBar() {
  const classes = useStyles();
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const isUserLoggedIn = useSelector((state) => state.userReducer).value != null

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const renderMobileMenu = (
    <StyledMenu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      keepMounted
      transformOrigin={{vertical: 'top', horizontal: 'right'}}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
      onClick={handleMobileMenuClose}
    >
      {menuItemsMain(isUserLoggedIn).map((value, index) => (
        <NavLink key={index} to={value.path}>
          <MenuItem>
            <ListItemIcon>
              {value.icon}
            </ListItemIcon>
            <ListItemText primary={value.title}/>
          </MenuItem>
        </NavLink>
      ))}
      <Hidden smUp>
        <Divider/>
        {menuItemsSecondary(isUserLoggedIn).map((value, index) => (
          <NavLink key={index} to={value.path}>
            <MenuItem>
              <ListItemIcon>
                {value.icon}
              </ListItemIcon>
              <ListItemText primary={value.title}/>
            </MenuItem>
          </NavLink>
        ))}
      </Hidden>
    </StyledMenu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <a href="https://www.minekloud.com" target="_blank" rel="noreferrer" className={classes.title}
             style={{marginRight: 50}}>
            <CloudCircle className={classes.menuButton}/>
            <Typography variant="h6" noWrap>
              Minekloud
            </Typography>
          </a>
          <div className={classes.sectionDesktop}>
            {menuItemsMain(isUserLoggedIn).map((value, index) => (
              <NavLink key={index} to={value.path}>
                <Button startIcon={value.icon} className={classes.menuButton} size="large" color="inherit">
                  {value.title}
                </Button>
              </NavLink>
            ))}
          </div>
          <div className={classes.grow}/>
          <div className={classes.sectionDesktop2}>
            {menuItemsSecondary(isUserLoggedIn).map((value, index) => (
              <NavLink key={index} to={value.path}>
                <Button startIcon={value.icon} className={classes.menuButton} size="large" color="inherit">
                  {value.title}
                </Button>
              </NavLink>
            ))}
          </div>
          <div className={classes.sectionMobile}>
            <IconButton onClick={handleMobileMenuOpen} color="inherit">
              <MoreIcon/>
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}